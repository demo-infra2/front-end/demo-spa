# Usa una imagen base, por ejemplo, un servidor Node.js
FROM node:16

# Establece el directorio de trabajo en el cual se copiarán los archivos de la aplicación
WORKDIR /app

COPY package*.json .

RUN npm install

COPY . .
# Expone el puerto en el cual la aplicación contenedor estará escuchando
EXPOSE 9000:9000

# Comando para iniciar la aplicación contenedor
CMD ["npm", "start"]
