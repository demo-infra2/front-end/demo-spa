import { registerApplication, start, LifeCycles, getAppStatus, triggerAppChange } from "single-spa";

registerApplication({
  name: "@uni/mf-menu",
  app: () =>
    System.import<LifeCycles>(
      "@uni/mf-menu"
    ),
  activeWhen: ["/"],
});

registerApplication({
  name: "@uni/mf-react",
  app: () =>
    System.import<LifeCycles>(
      "@uni/mf-react"
    ),
  activeWhen: ["/uni-demo-react","/menu#/uni-demo-react"],
});

registerApplication({
  name: "@uni/mf-vue",
  app: () =>
    System.import<LifeCycles>(
      "@uni/mf-vue"
    ),
  activeWhen: ["/uni-demo-vue","/menu#/uni-demo-vue"],
});

start({
  urlRerouteOnly: true,
});


export interface Microfrontend {
  mf: string;
  single: string;
}

const microfrontends: Microfrontend[] = [
  { mf: '@uni/mf-menu', single: 'single-spa-application:@uni/mf-menu' },
  { mf: '@uni/mf-react', single: 'single-spa-application:@uni/mf-react' },
  { mf: '@uni/mf-vue', single: 'single-spa-application:@uni/mf-vue' },
];

triggerAppChange().then(() => {
  for (let o of microfrontends) {
    isDisplay(o.mf, o.single);
  }
});

window.addEventListener('single-spa:app-change', (evt) => {
  for (let o of microfrontends) {
    isDisplay(o.mf, o.single);
  }
});


function isDisplay(nameMF: string, nameId: string) {
  const status = getAppStatus(nameMF);
  const usuario = document.getElementById(nameId);

  if (status == null || status == 'NOT_MOUNTED' || status == 'NOT_LOADED' || status == 'LOAD_ERROR') {
    //ocultando el componente no montado
    usuario.style.display = 'none';
  } else {
    usuario.style.display = 'block';
  }
}